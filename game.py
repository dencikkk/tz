import random
import pgzrun 

X = [-3 , -2 , -1 , 1 , 2 , 3] #скорости
Y = [-3 , -2 , -1 , 1 , 2 , 3] 
SPEED_X = random.choice(X)      # скорость по оси Х
SPEED_Y = random.choice(Y)      # скорость по оси У


WIDTH = 500   # ширина
HEIGHT = 500  # высота

GREEN = 0, 200, 0
box = Rect((20, 20), (100,100))  # создали объект




def draw(): # Отрисовали
	screen.fill((128, 0, 0 ))
	screen.draw.rect(box, GREEN)  # отрисовали объект


def update(): 
	global SPEED_X
	global SPEED_Y
	box.left += SPEED_X # двигаем по оси Х
	#проверка выхода за край право/лево
	if box.left >= WIDTH or box.right <=0 : SPEED_X = -SPEED_X
	box.top += SPEED_Y  # двигаем по оси У 
	#проверка выхода за край верх/низ
	if box.top >= HEIGHT or box.top <=0 : SPEED_Y = -SPEED_Y
	
pgzrun.go()